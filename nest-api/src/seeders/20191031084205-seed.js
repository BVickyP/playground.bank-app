'use strict';
const faker = require('faker');
const uuidv4 = require('uuid/v4');
const bcrypt = require('bcrypt');
const saltRounds = 10;

function createUsers(count) {
  let users = [];
  for (let i = 0; i < count; i++) {
    users.push({
      id: uuidv4(),
      name: faker.name.findName(),
      passport: bcrypt.hashSync(faker.internet.password(), saltRounds),
      email: faker.internet.email(),
      createdAt: new Date(),
      updatedAt: new Date()
    });
  }
  return users;
}

function createAccounts(count, usersId) {
  let accounts = [];
  for (let i = 0; i < count; i++) {
    accounts.push({
      id: uuidv4(),
      number: faker.finance.account(),
      balance: faker.random.number(),
      user_id: usersId[Math.floor(Math.random() * Math.floor(usersId.length))].id,
      createdAt: new Date(),
      updatedAt: new Date()
    });
  }
  return accounts;
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
      await queryInterface.bulkInsert('user', createUsers(10), {});
      const usersId = await queryInterface.sequelize.query(
        `SELECT id FROM "user";`
      );

      return await queryInterface.bulkInsert('account', createAccounts(20, usersId[0]), {});
  },

  down: async (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    await queryInterface.bulkDelete('account', null, {})
    return await queryInterface.bulkDelete('user', null, {});
  }
};
