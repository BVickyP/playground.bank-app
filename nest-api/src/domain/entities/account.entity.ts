import { Table, Column, Model, IsUUID, PrimaryKey, IsCreditCard, IsInt, Min, ForeignKey, AfterSync, BeforeSync } from 'sequelize-typescript';
import { User } from './user.entity';
import * as faker from 'faker';
import * as uuidv4 from 'uuid/v4';

@Table
export class Account extends Model<Account> {
  @IsUUID(4)
  @PrimaryKey
  @Column
  id!: string;

  @IsCreditCard// check for valid credit card numbers
  @Column
  number!: string;

  @IsInt
  @Min(-100)
  @Column
  balance: number;

  @ForeignKey(() => User)
  @Column
  userId: string;

  @BeforeSync
  static dropModel(instance: Account) {
    Account.drop();
  }

  @AfterSync
  static async seed(instance: Account) {
    const usersId = await instance.sequelize.query(`SELECT id FROM "Users";`);
    const accounts = createAccounts(20, usersId[0]);
    Account.bulkCreate(accounts);
  }
}

function createAccounts(count, usersId) {
  const accounts = [];
  for (let i = 0; i < count; i++) {
    accounts.push({
      id: uuidv4(),
      number: faker.finance.account(),
      balance: faker.random.number(),
      userId: usersId[Math.floor(Math.random() * Math.floor(usersId.length))].id,
      createdAt: new Date(),
      updatedAt: new Date(),
    });
  }
  return accounts;
}
