import { Table, Column, Model, IsUUID, PrimaryKey, IsEmail, HasMany, AfterSync, BeforeSync } from 'sequelize-typescript';
import { Account } from './account.entity';
import * as faker from 'faker';
import * as bcrypt from 'bcrypt';
import * as uuidv4 from 'uuid/v4';
const saltRounds = 10;

@Table
export class User extends Model<User> {
  @IsUUID(4)
  @PrimaryKey
  @Column
  id!: string;

  @Column
  name!: string;

  @Column
  passport!: string;

  @IsEmail
  @Column
  email!: string;

  @HasMany(() => Account)
  accounts: Account[];

  @BeforeSync
  static dropModel(instance: User) {
    User.drop({cascade: true});
  }

  @AfterSync
  static seed(instance: User) {
    User.bulkCreate(createUsers(10));
  }
}

function createUsers(count) {
  const users = [];
  for (let i = 0; i < count; i++) {
    users.push({
      id: uuidv4(),
      name: faker.name.findName(),
      passport: bcrypt.hashSync(faker.internet.password(), saltRounds),
      email: faker.internet.email(),
      createdAt: new Date(),
      updatedAt: new Date(),
    });
  }
  return users;
}
