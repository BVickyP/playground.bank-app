import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './modules/app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { UsersModule } from './modules/users.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());

  const usersOptions = new DocumentBuilder()
    .setTitle('User')
    .setDescription('The bank API description')
    .setVersion('1.0')
    .addTag('users')
    .build();
  const usersDocument = SwaggerModule.createDocument(app, usersOptions, {
    include: [UsersModule],
  });
  SwaggerModule.setup('api/users', app, usersDocument);

  const accountsOptions = new DocumentBuilder()
    .setTitle('Accounts')
    .setDescription('The bank API description')
    .setVersion('1.0')
    .addTag('accounts')
    .build();
  const accountsDocument = SwaggerModule.createDocument(app, accountsOptions, {
    include: [UsersModule],
  });
  SwaggerModule.setup('api/accounts', app, accountsDocument);

  await app.listen(4000);
}
bootstrap();
