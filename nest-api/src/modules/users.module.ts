import { Module } from '@nestjs/common';
import { UsersController } from '../infrastructure/api/users.controller';
import { UsersService } from '../services/users.service';
import { DatabaseModule } from './database.module';
import { User } from '../domain/entities/user.entity';
import * as consts from '../constants';

export const usersProviders = [
  {
    provide: consts.USERS_REPOSITORY,
    useValue: User,
  },
];

@Module({
  imports: [DatabaseModule],
  controllers: [UsersController],
  providers: [
    UsersService,
    ...usersProviders,
  ],
})
export class UsersModule {}
