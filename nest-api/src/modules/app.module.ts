import { Module } from '@nestjs/common';
import { UsersModule } from './users.module';
import { AccountsModule } from './accounts.module';

@Module({
  imports: [UsersModule, AccountsModule],
})
export class AppModule {}
