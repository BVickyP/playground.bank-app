import { Module } from '@nestjs/common';
import { Sequelize } from 'sequelize-typescript';
import { User } from '../domain/entities/user.entity';
import { Account } from '../domain/entities/account.entity';
import * as consts from '../constants';
import * as configDatabase from '../config/config.json';

const env = process.env.NODE_ENV || 'development';
const databaseProviders = [
  {
    provide: consts.SEQUELIZE,
    useFactory: async () => {
      const sequelize = new Sequelize(configDatabase[env]);
      sequelize.addModels([User, Account]);
      await sequelize.sync();
      return sequelize;
    },
  },
];

@Module({
  providers: [...databaseProviders],
  exports: [...databaseProviders],
})
export class DatabaseModule {}
