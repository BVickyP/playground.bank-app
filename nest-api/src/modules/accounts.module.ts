import { Module } from '@nestjs/common';
import { AccountsController } from '../infrastructure/api/accounts.controller';
import { AccountsService } from '../services/accounts.service';
import { DatabaseModule } from './database.module';
import { Account } from '../domain/entities/account.entity';
import * as consts from '../constants';

export const accountsProviders = [
  {
    provide: consts.ACCOUNTS_REPOSITORY,
    useValue: Account,
  },
];

@Module({
  imports: [DatabaseModule],
  controllers: [AccountsController],
  providers: [
    AccountsService,
    ...accountsProviders,
  ],
})
export class AccountsModule {}
