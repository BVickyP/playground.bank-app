import { Injectable, Inject } from '@nestjs/common';
import { User } from '../domain/entities/user.entity';
import * as consts from '../constants';

@Injectable()
export class UsersService {
  constructor(
    @Inject(consts.USERS_REPOSITORY) private readonly USERS_REPOSITORY: typeof User) {}

  async findAll(): Promise<User[]> {
    return await this.USERS_REPOSITORY.findAll<User>();
  }
}
