import { Injectable, Inject } from '@nestjs/common';
import { Account } from '../domain/entities/account.entity';
import * as consts from '../constants';

@Injectable()
export class AccountsService {
  constructor(
    @Inject(consts.ACCOUNTS_REPOSITORY) private readonly ACCOUNTS_REPOSITORY: typeof Account) {}

  async findAll(): Promise<Account[]> {
    return await this.ACCOUNTS_REPOSITORY.findAll<Account>();
  }
}
